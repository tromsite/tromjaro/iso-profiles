# TROMjaro ISO profile

Configuration to build latest TROMjaro ISO

**See adding packages to custom repo**: [Adding/Removing-Packages](https://gitlab.com/tromsite/tromjaro/iso-profiles/wikis/TROMjaro-Repository---Adding/Removing-Packages)

## Prerequisites

Install Manjaro Tools
To get started you need to the following meta-package:

```
pamac install manjaro-tools-iso git
```

Download the latest TROMjaro Profile:

```
git clone https://gitlab.com/tromsite/tromjaro/iso-profiles.git ~/iso-profiles
```


## Getting Started

The ISO profile for TROMjaro can now be found at:

```
~/iso-profiles/tromjaro/gnome
```
Let's take a closer look at the gnome profile folder (marked above):

```
**desktop-overlay**
live-overlay -> ../../shared/manjaro/live-overlay
**Packages-Desktop**
Packages-Live -> ../../shared/Packages-Live
Packages-Mhwd -> ../../shared/Packages-Mhwd
Packages-Root -> ../../shared/Packages-Root
**profile.conf**
```
- **desktop-overlay**: After every successful installation of your ISO the files and folders in here will get copied over.
- live-overlay: Similar to desktop-overlay. Files and folders in here will only get copied over to the LiveCD of your ISO.
- **Packages-Desktop**: This file contains packages for the ISO installation. All packages will also get installed on LiveCDs.
- Packages-Live: This file contains packages, which will only get installed on the LiveCD.
- Packages-Mhwd: This file contains Mhwd packages important to all Manjaro installations and LiveCDs.
- Packages-Root: This file contains base packages important to all Manjaro installations and LiveCDs.
- **profile.conf** contains basic settings for your ISO installation and LiveCD.

As you have probably noticed, several are only symlinks to the shared ISO profile. It is good practice to never change files or folders in the shared ISO profile. If you ever want to change them, first copy them to your ISO profile folder and delete the symlink.

All folders and files specific to your ISO are in bold. You can change them to your liking.

## Live boot hack

First backup:
```
mkdir ~/mysafespace && cp /etc/lsb-release ~/mysafespace/lsb-release
```
Then copy the following:

```
sudo cp ~/iso-profiles/tromjaro/lsb-release /etc/lsb-release
```
**IMPORTANT**

When build is complete please ensure to restore your lsb-release file:
```
sudo cp ~/mysafespace/lsb-release /etc/lsb-release
```

### Build your ISO

Build TROMjaro with the following command:

```
buildiso -p gnome
```
**Attention**: The build process needs at least 10 minutes to complete or much longer when you are using HDDs, slow CPUs, or large ISOs.

When the build process finishes successfully, the ISO file and the package list will appear in this folder: 

```
/var/cache/manjaro-tools/iso/
```

## Cleaning your hard drive

After a successful or failed build, you can get rid of most data (the "raw" ISO with all downloaded packages) by deleting this folder: 
```
sudo rm -r /var/lib/manjaro-tools/buildiso/
```
To clean your system of packages files of packages not installed on your system (this includes all the package files downloaded for your custom ISO): 
```
paccache -ruk0
```
You can also manually look into
```
/var/cache/manjaro-tools/
```
and delete folders or files to your liking. If you want to delete all ISO images, package lists, and cached Xorg packages do: 
```
sudo rm -r /var/cache/manjaro-tools/
```
Please remember that all these packages and files are saved for your convenience. If you clean your system like suggested above, you have to download **all** packages and build **all** images again the next time you want to build TROMjaro.

By default, your manjaro-tools.conf file is saved. If you want to delete it, use 

```
rm -r ~/.config/manjaro-tools
```

## Acknowledgments

* [Manjaro](https://manjaro.org/)
* [Manjaro gitlab](https://gitlab.manjaro.org/profiles-and-settings/iso-profiles)
* [Build Manjaro ISOs with buildiso](https://wiki.manjaro.org/Build_Manjaro_ISOs_with_buildiso)
* [Archlinux](https://www.archlinux.org/)
